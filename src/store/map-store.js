import { Store } from 'svelte/store.js';
export default class MapStore extends Store {
    async getEvents() {
        const eventsData =  await fetch('/events.json').then(data => data.json());
        this.set({events:eventsData.events});
        return this.get().events;
    }
}