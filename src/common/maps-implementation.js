export default class MapsImplementation {

    constructor(selector,options){
        this.selector = selector ;
        this.options = options;
        this.geocoder = new google.maps.Geocoder();
        this.map = new google.maps.Map(this.selector, this.options);  
    }

    geoCodeAddress(event){
        var self = this;
        this.geocoder.geocode({ 'address': event.address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                event.marker = self.createMarker(results[0].geometry.location);
            }
            else {
                console.log("some problem in geocode" + status);
            }

        });
        return event
    }

    createMarker(location){
        return new google.maps.Marker({
            map: this.map,
            position: location
        });
    }
}