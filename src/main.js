import App from './App.html';
import MainPage from './pages/main.html';
import 'bulma/css/bulma.css';
import 'whatwg-fetch'


const app = new App({
	target: document.body
});

export default app;